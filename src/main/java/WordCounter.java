import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

public class WordCounter {

    private Logger logger = LogManager.getLogger();
    private Map<String, Integer> wordCounts = new HashMap<String, Integer>();

    public Iterable<String> getTopFreqWords(SortingMethodType type, int N) {
        Long time = System.currentTimeMillis();
        logger.debug("Sorting with LINKED LIST...");

        Map<String, Integer> sortedByFreqMap = type.method.sort(wordCounts);
        Iterator<String> iter = sortedByFreqMap.keySet().iterator(); // returns Set view

        logger.debug("Execution time, ms: " + (System.currentTimeMillis() - time) /1000);

        Set<String> topFreqWords = new LinkedHashSet<String>();
        for (int i=0; i < N && iter.hasNext(); i++) {
            topFreqWords.add(iter.next());
        }

        return topFreqWords;
    }

    public void fillWords(Iterator<String> iter) {
        while (iter.hasNext()) {
            String word = iter.next();

            if (wordCounts.containsKey(word)) { // from jdoc: contains is preferable than get(word) == null
                Integer num = wordCounts.get(word);
                wordCounts.put(word, num+1);
            } else { //if (wordCounts.size() < N) {
                wordCounts.put(word, 0);
            }
        }
    }
}