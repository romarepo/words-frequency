import java.util.Map;

public interface SortingMethod {
    public <K extends Comparable,V extends Comparable> Map<K,V> sort(Map<K, V> map);
}
