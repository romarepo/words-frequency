import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class Init {
    public static final String TEST_FILE_PATH = "src/book1.txt";
    public static final int N = 10;

    public static final void main(String... args) {
        WordIterator wordIterator = new WordIterator();
        WordCounter wordCounter = new WordCounter();

        wordCounter.fillWords(wordIterator);
        Iterable<String> items = wordCounter.getTopFreqWords(SortingMethodType.LINKED_LIST, N);

        for (String item : items) {
            System.out.println(item);
        }
    }
}

class WordIterator implements Iterator<String> {
    private static final int BUF_SIZE = 1 *1024;

    private BufferedReader reader;
    private String[] lineWordList = null;
    private int index;

    public WordIterator() {
        try {
            reader = new BufferedReader(new FileReader(Init.TEST_FILE_PATH), BUF_SIZE);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    };

    @Override
    public boolean hasNext() {
        try {
            return reader.ready();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    };

    @Override
    public String next() {
        try {
            if (lineWordList == null || lineWordList.length == 0 || index == lineWordList.length-1) {
                lineWordList = makeWords(reader.readLine());
                index = 0;
            } else {
                index++;
            }
            return lineWordList[index];
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    };

    private String[] makeWords(String line) {
        if (line.contains(" ") && line.length() > 1) {
            return line.split(" ");
        } else {
            return new String[]{""};
        }
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}

@Deprecated
class WordNioIterator implements Iterator<String> {

    private static FileInputStream fIn;
    private static FileChannel fChan;
    private static long fSize;
    private static ByteBuffer mBuf;

    private static Charset charset = Charset.forName("UTF-8");
    private static CharsetDecoder decoder = charset.newDecoder();

    private int i = 0;

    public WordNioIterator() throws IOException {
        fIn = new FileInputStream(Init.TEST_FILE_PATH);
        fChan = fIn.getChannel();
        fSize = fChan.size();
        mBuf = ByteBuffer.allocate((int) fSize);
        fChan.read(mBuf);
        mBuf.rewind();
    }

    @Override
    public boolean hasNext() {
        // TODO:
        try {
            fChan.close();
            fIn.close();
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String next() {
        try {
            i++;
//          mBuf.flip();
            return decoder.decode(mBuf).toString();
//          mBuf.clear();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}