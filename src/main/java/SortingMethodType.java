import com.google.common.collect.Maps;
import com.google.common.collect.Ordering;

import java.util.*;

public enum SortingMethodType {
    GUAVA_NATURAL(new SortingMethod() {
        @Override
        public <K extends Comparable, V extends Comparable> Map<K, V> sort(final Map<K, V> map) {
            // unfortunately, TreeMap cannot
            Map<K, V> sortedMap = Maps.newTreeMap(new Comparator<K>() {
                @Override
                public int compare(K o1, K o2) {
                    V i1 = map.get(o1);
                    V i2 = map.get(o2);

                    return i2.compareTo(i1);
                }
            });
            sortedMap.putAll(map);

            return sortedMap;
        }
    }), LINKED_LIST(new SortingMethod() {
        @Override
        public <K extends Comparable, V extends Comparable> Map<K, V> sort(Map<K, V> map) {
            List<Map.Entry<K, V>> items = new LinkedList<Map.Entry<K, V>>(map.entrySet());
            Collections.sort(items, new Comparator<Map.Entry<K,V>>() {
                @Override
                public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                    return o2.getValue().compareTo(o1.getValue()); // DESC order
                }
            });

            // provides predictable iteration order
            Map<K,V> sortedMap = new LinkedHashMap<K, V>();

            for(Map.Entry<K,V> item: items) {
                sortedMap.put(item.getKey(), item.getValue());
            }

            return sortedMap;
        }
    });

    SortingMethod method;

    SortingMethodType(SortingMethod method) {
        this.method = method;
    }
}