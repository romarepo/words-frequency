import java.util.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.*;

public class SortingMethodTest {

    private Logger logger = LogManager.getLogger();
    private static final int COUNT = 1000 *1000;

    @Test
    public void testSortWithGuavaByValue() {
        Random random = new Random(System.currentTimeMillis());
        Map<String, Integer> testMap = new HashMap<String, Integer>(COUNT);
        for (int i = 0; i < COUNT; ++i) {
            testMap.put("String" + random.nextInt(), random.nextInt());
        }

        Long time = System.currentTimeMillis();
        logger.debug("Sorting with GUAVA COLLECTION...");

        testMap = SortingMethodType.GUAVA_NATURAL.method.sort(testMap);

        logger.debug("Execution time, ms: " + (System.currentTimeMillis() - time) /1000);

        Integer previous = null;
        for (Map.Entry<String, Integer> entry : testMap.entrySet()) {
            Assert.assertNotNull(entry.getValue());
            if (previous != null) {
                Assert.assertTrue(entry.getValue() <= previous); // sort DESC
            }
            previous = entry.getValue();
        }
    }

    @Test
    public void testSortWithLinkedListByValue() {
        Random random = new Random(System.currentTimeMillis());
        Map<String, Integer> testMap = new HashMap<String, Integer>(COUNT);
        for (int i = 0; i < COUNT; ++i) {
            testMap.put("String" + random.nextInt(), random.nextInt());
        }

        Long time = System.currentTimeMillis();
        logger.debug("Sorting with LINKED LIST...");

        testMap = SortingMethodType.LINKED_LIST.method.sort(testMap);

        logger.debug("Execution time, ms: " + (System.currentTimeMillis() - time) /1000);

        Integer previous = null;
        for (Map.Entry<String, Integer> entry : testMap.entrySet()) {
            Assert.assertNotNull(entry.getValue());
            if (previous != null) {
                Assert.assertTrue(entry.getValue() <= previous); // sort DESC
            }
            previous = entry.getValue();
        }
    }

    @Test
    public void testCountingOfWords() {
        String[] input = "Hi now Hi there now now Olympic there Olympic Hi Hi".split(" ");

        WordCounter processing = new WordCounter();
        processing.fillWords(Arrays.asList(input).iterator());        
        Iterable<String> items = processing.getTopFreqWords(SortingMethodType.LINKED_LIST, 2);

        Iterator<String> iter = items.iterator();
        String item1 = iter.next();
        String item2 = iter.next();

        Assert.assertTrue(item1.equals("Hi") || item1.equals("now"));
        Assert.assertFalse(item1.equals("there"));
        Assert.assertTrue(item2.equals("Hi") || item2.equals("now"));
        Assert.assertFalse(item2.equals("there"));

        Assert.assertFalse(iter.hasNext());
    }
}